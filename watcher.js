var respawn = require('respawn');

[1].forEach((v) => {
  var monitor = respawn([
    'ffmpeg.exe',
    '-rtsp_transport', 'tcp',
    `-i`, `rtsp://192.168.10.${v}`,
    '-c:v', 'h264',
    '-flags', '+cgop',
    '-g', '30',
    '-hls_flags', 'delete_segments',
    '-hls_time', '10',
    `${v}/live.m3u8`
  ], {
    name: `live${v}`,          // set monitor name
    cwd: '.',              // set cwd
    maxRestarts:-1,        // how many restarts are allowed within 60s
    // or -1 for infinite restarts
    sleep:1000,            // time to sleep between restarts,
    kill:30000,            // wait 30s before force killing after stopping
  });

  monitor.start();
});
