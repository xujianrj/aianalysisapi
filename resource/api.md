**简要描述：**
- 获取事件列表

**请求方法：**
- Get Method
- api/events
- 分页示例：
http://localhost:3000/api/events?pageIndex=1&pageSize=20
- 按时间搜索示例：
http://localhost:3000/api/events?enterStartTime=2019-11-10 15:18:31&enterEndTime=2019-11-10 15:19:30

**参数：**

参数名|必须|类型|说明
-|-|-|-
pageIndex|NO|int|分页页码（从0开始）
pageSize|NO|int|单页条目数量(默认10)
enterStartTime|NO|string|搜索条件：进入的起始时间
enterEndTime|NO|string|搜索条件：进入的截止时间
leaveStartTime|NO|string|搜索条件：离开的起始时间
leaveEndTime|NO|string|搜索条件：离开的截止时间



**返回数据：**
```
{
    "code": 0,
    "message": "OK",
    "data": 
        [
          {
            faceId: "a0eb6ca6-4645-4eaf-8f37-272ebe8b10e8",
            enterTime: "2019-01-01 10:11:12",
            leaveTime: "2019-01-01 11:11:12",
            imageKey: "1573095726540"，
            imageFullPath："/resource/e842c1bf-b192-428b-83c4-3e4eab65d382/1573191836541.png"，
            videoFullPath": "/resource/e842c1bf-b192-428b-83c4-3e4eab65d382/1573191836541.mp4"
          }
        ]
    }
}
```

**返回字段说明：**

字段名 | 类型 | 说明
-|-|-
faceId|string|当前事件的唯一ID
enterTime|datetime|进入时间
leaveTime|datetime|离开时间
imageKey|string|资源关键标识，定位资源文件
imageFullPath|string|图像地址完整路径
videoFullPath|string|视频地址完整路径

图像预览示例：
http://localhost:3000/resource/e842c1bf-b192-428b-83c4-3e4eab65d382/15730957265402.png
