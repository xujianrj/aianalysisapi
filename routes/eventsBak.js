let express = require('express');
let router = express.Router();
const moment = require('moment');
const CommonHelper = require('../utils/commonHelper');
const connection = require('../utils/mysqlDB');


router.get('/enter', function (req, res, next) {
  let enterTime = new Date();
  let faceId = req.query.faceId;
  let enterTimeText = moment(enterTime).format('YYYY-MM-DD HH:mm:ss');
  let addSql = 'INSERT INTO event(faceId,enterTime,imageKey) VALUES(?,?,?)';
  let addSqlParams = [ faceId, enterTimeText, '', '' ];
  connection.query(addSql, addSqlParams, function (err, result) {
    if (err) {
      console.log('[INSERT ERROR] - ', err.message);
      res.send(err.message);
      return;
    }
    console.log('INSERT ID:', result);
    res.send(result);
  });
});
router.get('/leave', function (req, res, next) {
  let modSql = 'UPDATE event SET leaveTime = ?,imageKey = ? WHERE faceId = ?';
  let leaveTime = new Date();
  let leaveTimeText = moment(leaveTime).format('YYYY-MM-DD HH:mm:ss');
  let modSqlParams = [ leaveTimeText, new Date().getTime(), req.query.faceId ];
  connection.query(modSql, modSqlParams, function (err, result) {
    if (err) {
      console.log('[INSERT ERROR] - ', err.message);
      res.send(err.message);
      return;
    }
    console.log('INSERT ID:', result);
    res.send(result);
  });
});
router.get('/insert', function (req, res, next) {
  let faceId = req.query.faceId ? req.query.faceId : CommonHelper.uuid();
  let leaveTimeText = req.query.leaveTime;
  let enterTimeText = req.query.enterTime;
  let imageKey = req.query.imageKey ? req.query.imageKey : new Date().getTime();
  let addSql = 'INSERT INTO event(faceId,enterTime,leaveTime,imageKey) VALUES(?,?,?,?)';
  let addSqlParams = [ faceId, enterTimeText, leaveTimeText, imageKey ];
  connection.query(addSql, addSqlParams, function (err, result) {
    if (err) {
      console.log('[INSERT ERROR] - ', err.message);
      res.send(err.message);
      return;
    }
    console.log(result.insertId);
    if (result.insertId) {
      connection.query(`SELECT * from event where id=${result.insertId}`,
        function (queryError, queryResults, fields) {
          res.send(queryResults);
        })
    }
  });
});




module.exports = router;


