let express = require('express');
let router = express.Router();
const moment = require('moment');
const CommonHelper = require('../utils/commonHelper');
const connection = require('../utils/mysqlDB');


router.get('/', function (req, res, next) {
  let pageIndex = req.query.pageIndex ? req.query.pageIndex : 0;
  let pageSize = req.query.pageSize ? req.query.pageSize : 10;
  let enterStartTimeQuery = req.query.enterStartTime ? ` and enterTime >= '${req.query.enterStartTime}'` : '';
  let enterEndTimeQuery = req.query.enterEndTime ? ` and enterTime <= '${req.query.enterEndTime}'` : '';
  let leaveStartTimeQuery = req.query.leaveStartTime ? ` and leaveTime >= '${req.query.leaveStartTime}'` : '';
  let leaveEndTimeQuery = req.query.leaveEndTime ? ` and leaveTime <= '${req.query.leaveEndTime}'` : '';
  let itemIndex = pageIndex * pageSize;
  let querySql=`SELECT * from event  where 1=1 `
    + enterStartTimeQuery + enterEndTimeQuery + leaveStartTimeQuery + leaveEndTimeQuery +
    ` order by enterTime desc limit ${itemIndex},${pageSize}`;
  console.log('querySql',querySql);
  connection.query(querySql,
    function (error, results, fields) {
      if (error) throw error;
      let normalResponse = {code: 0, message: 'ok'};
      results.forEach(s => {
        if (s.imageKey) {
          s.imageFullPath = `/resource/${s.faceId}/${s.imageKey}.png`;
          s.videoFullPath = `/resource/${s.faceId}/${s.imageKey}.mp4`;
        } else {
          s.imageFullPath = `/assets/unknown.png`;
          s.videoFullPath = ``;
        }
      });
      normalResponse.data = results;
      res.send(normalResponse);
    });
});


router.get('/createSampleData', function (req, res, next) {
  let dateTimeNow = new Date();
  let enterTime = new Date();
  let leaveTime = new Date();
  leaveTime.setSeconds(leaveTime.getSeconds() + 30);
  let leaveTimeText = moment(leaveTime).format('YYYY-MM-DD HH:mm:ss');
  let enterTimeText = moment(enterTime).format('YYYY-MM-DD HH:mm:ss');
  let addSql = 'INSERT INTO event(faceId,enterTime,leaveTime,imageKey) VALUES(?,?,?,?)';
  let addSqlParams = [ CommonHelper.uuid(), enterTimeText, leaveTimeText, dateTimeNow.getTime() ];
  connection.query(addSql, addSqlParams, function (err, result) {
    if (err) {
      console.log('[INSERT ERROR] - ', err.message);
      res.send(err.message);
      return;
    }
    console.log('INSERT ID:', result);
    res.send(result);
  });
});


module.exports = router;


