let express = require('express');
let router = express.Router();
let mysql = require('mysql');
// import CommonHelper from '../utils/commonHelper';

const moment = require('moment');
const CommonHelper = require('../utils/commonHelper');


let connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'ai',
  timezone: "00:00"
});

connection.connect();

module.exports =connection;
