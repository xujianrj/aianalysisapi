const connection = require('../utils/mysqlDB');
const moment = require('moment');
const CommonHelper = require('../utils/commonHelper');


module.exports = {
  createSampleData: () => {
    let dateTimeNow = new Date();
    let enterTime = new Date();
    let leaveTime = new Date();
    leaveTime.setSeconds(leaveTime.getSeconds() + 30);
    let leaveTimeText = moment(leaveTime).format('YYYY-MM-DD HH:mm:ss');
    let enterTimeText = moment(enterTime).format('YYYY-MM-DD HH:mm:ss');
    let addSql = 'INSERT INTO event(faceId,enterTime,leaveTime,imageKey,rtspAddress) VALUES(?,?,?,?,?)';
    let addSqlParams = [ CommonHelper.uuid(), enterTimeText, leaveTimeText, dateTimeNow.getTime(),'mock' ];
    connection.query(addSql, addSqlParams, function (err, result) {
      if (err) {
        console.log('[INSERT ERROR] - ', err.message);
        return;
      }
      console.log('INSERT ID:', result);
    });
  }
};
