import argparse
import datetime

import ffmpeg


# python3 clip_video.py --rtsp rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov --duration 10 --count 4



# ------------------------------------------------------------------------------
if __name__ == "__main__":

    # USAGE
    # $ python collect_and_store.py --rtsp rtsp://user:pass1@71.85.125.110:554 \
    #       --duration 30 --count 10

    # construct the argument parser and parse the arguments
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument("--rtsp",
                             required=True,
                             type=str,
                             help="RTSP URL for video stream")
    args_parser.add_argument("--duration",
                             required=True,
                             type=int,
                             help="duration of saved clips (in seconds)")
    args_parser.add_argument("--count",
                             required=True,
                             type=int,
                             help="number of clips to save")
    args = vars(args_parser.parse_args())

    # sanity check for some of the arguments
    if not args["rtsp"].lower().startswith("rtsp://"):
        raise ValueError("Invalid input URL -- only RTSP supported")

    seconds_per_clip = args["duration"]
    start = int(datetime.datetime.now().strftime("%s"))
    end = start + seconds_per_clip
    number_of_files_to_collect = args["count"]

    while number_of_files_to_collect > 0:

        # build URL with start and end times
        # NOTE URL is for Uniview RTSP, add options for other camera types

        url = args["rtsp"]
        # url = args["rtsp"] + f"/c1/b{start}/replay/"
        # url = args["rtsp"] + f"/c1/b{start}/e{end}/replay/"

        # file where we'll write clip data
        temp_file = f"clip_b{start}_e{end}.mp4"

        # create the equivalent of the ffmpeg command:
        # $ ffmpeg -i <rtsp_url> -vcodec copy -y -rtsp_transport tcp <output_mp4>
        stream = ffmpeg.input(url)
        stream = ffmpeg.output(stream, temp_file,
                               **{"codec:v": "copy",
                                  "rtsp_transport": "tcp",
                                  "t": f"00:00:{str(seconds_per_clip).zfill(2)}",
                                  "y": None
                                  }
                               )
        ffmpeg.run_async(stream)

        print(f"\n\nMP4 file created: {temp_file}")

        number_of_files_to_collect -= 1
        start += seconds_per_clip
        end += seconds_per_clip

    print('finished-------------------')
    main_thread_test = 10
    while main_thread_test > 0:
        print('finished-------------------{main_thread_test}')
        main_thread_test-=1
